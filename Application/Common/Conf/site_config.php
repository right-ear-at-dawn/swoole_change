<?php
/**
 * Copyright (c) 2015-2016 http://www.uminicmf.com All rights reserved.
 * Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * Author：      杜二红 <1186969412@qq.com>
 * Created by:  2018/08/01
 *
 * ----------------------------
 * 网站系统配置文件
 * 单个应用请在这里写配置信息
 * ----------------------------
 */
return array(
	'APP_SUB_DOMAIN_DEPLOY'   =>    1, // 开启子域名配置
	'APP_SUB_DOMAIN_RULES'    =>    array(   
	    'm.meike.com'  => 'Wap',
	),
	"DIC_POSITION"=>array(
		"1"=>"全局",
		"2"=>"首页",
		"3"=>"个人中心页",
	),


	// 自动续期
	"AUTO_PERIOD"=>true,

	"icon_url"=>"http://at.alicdn.com/t/font_673726_0jeocuq66it9ms4i.css",
);
