<?php
/**
 * Copyright (c) 2015-2016 http://www.uminicmf.com All rights reserved.
 * Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * Author：      杜二红 <1186969412@qq.com>
 * Created by:  2018/08/01
 *
 * ----------------------------
 * 数据库配置文件
 * ----------------------------
 */
return array(
	//'配置项'=>'配置值'
	'DB_TYPE'   => 'mysql', // 数据库类型

	'DB_HOST'   => '115.159.58.184', // 服务器地址
	'DB_NAME'   => 'umini', // 数据库名
	'DB_USER'   => 'fang', // 用户名
	'DB_PWD'    => 'fang', // 密码

	'DB_PORT'   => 3306, // 端口
	'DB_PREFIX' => 'umini_',
);
