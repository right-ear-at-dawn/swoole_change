<?php  
/**
 * Copyright (c) 2015-2016 http://www.uminicmf.com All rights reserved.
 * Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * Author：      杜二红 <1186969412@qq.com>
 * Created by:  2018/08/01
 *
 * ----------------------------
 * 网站前台主函数库
 * ----------------------------
 */

//--------------------------- 系统函数库 --------------------------

//PHP获取当前页面完整路径URL
function getFullUrl(){
    # 解决通用问题
    $requestUri = '';
    if (isset($_SERVER['REQUEST_URI'])) {
        $requestUri = $_SERVER['REQUEST_URI'];
    } else {
        if (isset($_SERVER['argv'])) {
            $requestUri = $_SERVER['PHP_SELF'] .'?'. $_SERVER['argv'][0];
        } else if(isset($_SERVER['QUERY_STRING'])) {
            $requestUri = $_SERVER['PHP_SELF'] .'?'. $_SERVER['QUERY_STRING'];
        }
    }
    $scheme = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
    $protocol = strstr(strtolower($_SERVER["SERVER_PROTOCOL"]), "/",true) . $scheme;
       //端口还是蛮重要的，毕竟需要兼容特殊的场景
    $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
    # 获取的完整url
    $_fullUrl = $protocol . "://" . $_SERVER['SERVER_NAME'] . $port . $requestUri;
    return $_fullUrl;
}

// 字典配置，全局字典信息
function dic($key,$field=false){
    $map=array();
    $map['sign']=$key;
    $data=M("dic")->where($map)->find();
    if ($data) {
        if ($field) {
            return $data[$field];
        } else {
            return $data["title"];
        }
        
    } else {
        return $key;
    }
}

// --------------------self------------------
