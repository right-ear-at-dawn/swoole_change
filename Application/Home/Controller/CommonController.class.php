<?php
/**
 * Copyright (c) 2015-2016 http://www.uminicmf.com All rights reserved.
 * Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * Author：      杜二红 <1186969412@qq.com>
 * Created by:  2018/08/01
 *
 * ----------------------------
 * 网站前台基础控制器
 * ----------------------------
 */
namespace Home\Controller;
use Think\Controller;
class CommonController extends Controller{
	// 初始化获取菜单，公共配置项
	public function _initialize(){
		// 获取菜单
		$mmap=array();
		$mmap['is_menu']=1;
		$this->menu_list=M('m_menu')->field('cat_name,path')->where($mmap)->order("orderid asc")->select();

		// 关于菜单
		$this->secMenu=M('m_menu')->where('parent_id=39')->order('orderid asc')->select();
		// 当前路径
		$this->curr_path="Home/".$Think.CONTROLLER_NAME.'/'.$Think.ACTION_NAME;

		// 当前菜单
		$this->curr_menu=M("m_menu")->field("id,to_order,page_num,cat_name")->where("path='".$Think.CONTROLLER_NAME.'/'.$Think.ACTION_NAME."'")->find();

		// 不存在则去匹配一级目录
		if (!$this->curr_menu) {
			foreach ($this->menu_list as $key => $row) {
				$row_node = explode("/", $row['path']);
				if ($row_node[0]==$Think.CONTROLLER_NAME) {
					$this->curr_menu=$row;
				}
			}
		}
		
		// banner图
		$this->banner=M('banner')->field('title,img,img_small,urls')->order($this->asc)->select();
		$this->about_list=M('m_menu')->where('parent_id=39')->order('orderid asc,id asc')->select();

		$this->siteInfo=M('site')->find();
	}
}