<?php if (!defined('THINK_PATH')) exit();?><html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <META HTTP-EQUIV="pragma" CONTENT="no-cache">
  <meta http-equiv="pragma" content="no-store">
  <META HTTP-EQUIV="Cache-Control" CONTENT="no-cache, must-revalidate">
  <META HTTP-EQUIV="expires" CONTENT="0">


  

  <title><?php echo ($site_info['site_title']); ?> <?php echo ($version); ?> </title>
  <link href="/Public/boot/css/bootstrap.min.css" rel="stylesheet">
  <link href="/Public/<?php echo C(DEFAULT_THEME);?>/Static/css/myboot.css" rel="stylesheet" type="text/css" media="screen">
  <link href="/Public/<?php echo C(DEFAULT_THEME);?>/Static/css/base.css" rel="stylesheet" type="text/css" media="screen">
  <link rel="stylesheet" type="text/css" href="/Public/plug/datetimepicker/jquery.datetimepicker.css"/>

  <link href="/Public/icon/iconfont.css" rel="stylesheet">
  <link href="http://at.alicdn.com/t/font_767888_dacrmbkok1.css" rel="stylesheet">

  

  <script src="/Public/boot/js/jquery.js"></script>

  <script src="/Public/plug/datetimepicker/build/jquery.datetimepicker.full.js"></script>
  <script>




  //实现后退刷新页面
  $.datetimepicker.setLocale('zh');
  </script>
  <?php
 if(strpos($HTTP_SERVER_VARS[HTTP_USER_AGENT], "MSIE 8.0")) { echo ' <link href="/Public/<?php echo C(DEFAULT_THEME);?>/Static/css/ie.css" rel="stylesheet" type="text/css" media="screen">'; } ?>
  
</head>



<body onbeforeunload="return hidden_msg()">

<style type="text/css">
  .iconfont{
    font-size: 17px;
  }
</style>


<!-- 头部功能菜单 -->
<div class="top">

<!-- 必要时，可以替换header -->


<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><img src="<?php echo ($site_info['site_logo']); ?>" width="20"></a>
      <a class="navbar-brand" href="#"><?php echo ($site_info['site_title']); ?> <small><?php echo ($version); ?></small></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">

        <?php if($PUB_TOPMENU_LIST != 0): if(is_array($PUB_TOPMENU_LIST)): foreach($PUB_TOPMENU_LIST as $key=>$row): ?><li class="<?php echo (top_menu_active($row['id'],$PUB_THISMENU['id'])); ?>">
                <?php if($row['url'] != '0'): ?><a class="model_name" href="<?php echo U($row['url']);?>">
                    <span class="<?php echo ($row['img']); ?>" aria-hidden="true"></span>
                    &nbsp;&nbsp;<?php echo ($row['title']); ?>
                  </a>
                <?php else: ?>
                  <a class="model_name" href="<?php echo U($row['node_name'].'/Index/index');?>">
                    <span class="<?php echo ($row['img']); ?>" aria-hidden="true"></span>
                    &nbsp;&nbsp;<?php echo ($row['title']); ?>
                  </a><?php endif; ?>
              </li><?php endforeach; endif; endif; ?>


      </ul>





      <ul class="nav navbar-nav navbar-right header-tools">
        <li><img src="/Public/default/Static/images/avatar2.jpg" class="portrait"></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo ($_SESSION['user']['username']); ?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">修改个人资料</a></li>
            <li><a href="#">修改密码</a></li>
          </ul>
        <li><a href="<?php echo U('Auth/Index/logout');?>">注销系统</a></li>
      </ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>






</div>


<div class="row top50">
    <div class="sleft">
    	<div class="menu">
        
          <!-- 侧栏列表 -->
          <div class="panel-group cardsecmenu" id="sec_menu" role="tablist" aria-multiselectable="true">
 <?php if(is_array($SEC_MENU)): foreach($SEC_MENU as $key=>$row): if($row['type'] == 1): ?><div class="panel panel-default">
      <div class="panel-heading" role="tab" id="sec_menu_<?php echo ($row['id']); ?>">
        <h4 class="panel-title">
          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sec_menu" href="#link_sec_menu_<?php echo ($row['id']); ?>" aria-expanded="false" aria-controls="#link_sec_menu_<?php echo ($row['id']); ?>">
            <span class="<?php echo ($row['img']); ?>" aria-hidden="true"></span>
              &nbsp;&nbsp;<?php echo ($row['title']); ?>
          </a>
        </h4>
      </div>
      <?php if(find_sec_menu($PUB_THISMENU['id'])['node_name'] == $row['node_name']): ?><div id="link_sec_menu_<?php echo ($row['id']); ?>" class="panel-collapse collapse  in" role="tabpanel" aria-labelledby="sec_menu_<?php echo ($row['id']); ?>">
      <?php else: ?>
        <div id="link_sec_menu_<?php echo ($row['id']); ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="sec_menu_<?php echo ($row['id']); ?>"><?php endif; ?>
        <div class="panel-body">

          <!-- 这里判断页面内容 -->
          <?php if($row['node_name'] == 'Site/Content'): ?><div id="treeview" class="treeview">
               <ul class="list-group">
                
               </ul>
              </div><?php endif; ?>




            <?php $next_menu=get_menu(array('type'=>'2','pid'=>$row['id'])) ?>
            <?php if(is_array($next_menu)): foreach($next_menu as $key=>$row1): if($row1['node_name'] == $PUB_NODE): ?><a type="button" class="link next_active" href="<?php echo U($row1['node_name']);?>">
              <?php else: ?>
                <a type="button" class="link" href="<?php echo U($row1['node_name']);?>"><?php endif; ?>
                <span class="<?php echo ($row1['img']); ?>" aria-hidden="true"></span>
                &nbsp;&nbsp;<?php echo ($row1['title']); ?>
              </a><?php endforeach; endif; ?>
        </div>
      </div>
    </div>
    
    <?php else: ?>
      <div class="panel panel-default cpanel">
        <div class="panel-heading">
          <h4 class="panel-title">
            <?php if($row['node_name'] == $PUB_NODE or $row['node_name'] == default_model($PUB_NODE)): ?><a class="" style="color: #BB5B5B" href="<?php echo U($row['node_name']);?>">
            <?php else: ?>
              <a class="" href="<?php echo U($row['node_name']);?>"><?php endif; ?>
              <span class="<?php echo ($row['img']); ?>"></span>
              &nbsp;&nbsp;<span class="unopen"><?php echo ($row['title']); ?></span>
              </a>
          </h4>
        </div>
      </div><?php endif; endforeach; endif; ?>
</div>



        
      </div>
    </div>

  	<div class="main">
      <!-- 右侧标题区 [必须]-->
      <div class="main_map">
        
          <ol class="breadcrumb">
            <?php echo ($path_nav); ?>
            <?php if($back_url != false): ?><li style="float:right"><a href="<?php echo ($back_url); ?>" class="btn btn-success btn-xs">返回上页</a></li><?php endif; ?>

          </ol>
        
      </div>

      <div class="main_body">
        <!-- 工具条【可选】 -->
        

        <!-- 主要内容区域【可选】 -->
        
<link href="/Public/default/Static/css/mindex.css" rel="stylesheet">
	<div class="row index_main">
		<div class="col-md-8">
    		<div class="logs_main">
    			<div class="logs_tt"><h4>欢迎您，<?php echo ($_SESSION['user']['username']); ?></h4></div>

                <table class="table table-bordered">
                    <tr>
                        <th width="80">版本</th>
                        <td>UminiCMF3.0</td>

                        <th width="80">数据库</th>
                        <td>Mysql</td>
                    </tr>

                    <tr>
                        <th width="80">官网</th>
                        <td>www.uminicmf.com</td>

                        <th width="80">系统</th>
                        <td>UminiCMF3.0</td>
                    </tr>

                    <tr>
                        <th width="80">版权</th>
                        <td>UminiCMF开发团队</td>

                        <th width="80">环境</th>
                        <td>LAMP（Linux+Apache+Mysql+PHP）</td>
                    </tr>

                    
                </table>


                <div class="logs_tt top30"><h4>开发团队</h4></div>
                <div class="logs_total">
					<table class="table table-striped">
                      <tr>
                        <td width="100">开发团队</td>
                        <td>UminiCMF团队</td>
                      </tr>
                      <tr>
                        <td>QQ</td>
                        <td>888888888</td>
                      </tr>
                      <tr>
                        <td>微信</td>
                        <td>uminicmf</td>
                      </tr>
                      <tr>
                        <td>邮箱</td>
                        <td>1186969412@qq.com</td>
                      </tr>
                    </table>
                </div>



    		</div>  <!-- end logs_main -->
    	</div>

    	<div class="col-md-4">
    		<div class="logs_lists">
    			<div class="logs_item">
    				<h4>最近操作</h4>
    			</div>

                <?php if(is_array($log_lists)): foreach($log_lists as $key=>$row): ?><div class="logs_item">
    				<div class="logs_action">
    					对【<?php echo (get_model_name($row['model_name'])); ?>】进行了【<?php echo ($row['action']); ?>】操作
    				</div>
    				<div class="logs_times">
    					IP:<?php echo ($row['ip']); ?>&nbsp;时间:<?php echo ($row['times']); ?>
    				</div>
    			</div><?php endforeach; endif; ?>
    		</div>
    	</div>
	</div>

      </div><!-- main_body -->
    </div>
</div>

<div class="alt_msg" style="display: none;">
  
</div>



  <div class="footer">
版权归UminiCMF开发团队所有
</div>

<script src="/Public/boot/js/bootstrap.min.js"></script>
<script src="/Public/plug/datetime/bootstrap-datetimepicker.js" ></script>
<script src="/Public/plug/datetime/bootstrap-datetimepicker.zh-CN.js"></script>
<script>
$(".datetimepicker").datetimepicker({
    language:  "zh-CN",
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    forceParse: 0,
    showMeridian: 1
});

$(".datepicker").datetimepicker({
    language:  "zh-CN",
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    forceParse: 0,
    showMeridian: 1
});
</script>

<script src="/Public/default/Static/js/my.js"></script>

</body>
</html>






<script type="text/javascript">
$.get("<?php echo U('System/Index/message_info');?>", function(result){
  if (result.code==1) {
    var str="";
    str=str+"<div class='show_msg'>";
    str=str+result.info;
    str=str+"</div>";
    $('.alt_msg').html(str);
    $('.alt_msg').show();
    $('.alt_msg').fadeOut(2000);
  }
});

function switch_dbs(key){
  $url="<?php echo U('System/Api/switch_server');?>?key="+key;
  $.get($url, function(result){
    if (result.code==0) {
      alert(result.info);
      window.location.reload();
    }
    else{
      alert(result.info);
    }
    
  });
}

</script>

<!-- 树形插件 -->

<script type='text/javascript' src='/Public/plug/btree/js/bootstrap-treeview.js'></script>
<script>
  $('#treeview').treeview({
    color: '#428bca',
    expandIcon: 'glyphicon glyphicon-chevron-right',
    collapseIcon: 'glyphicon glyphicon-chevron-down',
    // nodeIcon: 'iconfont icon-wenjianjia',
    emptyIcon:'iconfont icon-wenjian',
    data: <?php echo ($site_menu); ?>,
    enableLinks:true,
    levels:2,
    highlightSelected:false,
  });
  
  // $('#treeview').treeview('expandNode', [ 0, {levels:2, silent:true} ]);
</script>


</body>
</html>