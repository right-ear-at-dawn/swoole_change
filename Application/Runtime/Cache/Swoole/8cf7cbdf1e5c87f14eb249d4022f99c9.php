<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <title>客户端1</title>
    <meta charset="UTF-8">
    <script src="http://libs.baidu.com/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://files.cnblogs.com/files/zenghansen/jquery.json-2.3.min.js"></script>
    <!--/*js地址：https://files.cnblogs.com/files/zenghansen/jquery.json-2.3.min.js*/-->
    <script type="text/javascript">
        var fid = '1'; //发送者uid
        var tid = '2'; //接收者uid
        var url='ws://115.159.58.184:9502';


        var exampleSocket=new WebSocket(url);
        $(function () {
            exampleSocket.onopen = function (event) {
                console.log(event.data);
                initData(); //加载历史记录
            };
            exampleSocket.onmessage = function (event) {
                console.log(event.data);
                loadData($.parseJSON(event.data)); //导入消息记录，加载新的消息
            }


        })
        function sendMsg() {
            var pData = {
                content: document.getElementById('content').value,
                fid: fid,
                tid: tid,
            }
            if(pData.content == ''){
                alert("消息不能为空");
                return;
            }
            exampleSocket.send(JSON.stringify(pData)); //发送消息
        }
        function initData() {
            var pData = {
                fid: fid,
                tid: tid,
            }
            exampleSocket.send(JSON.stringify(pData)); //获取消息记录，绑定fd
        }
        function loadData(data) {
            for (var i = 0; i < data.length; i++) {
                var html = '<p>' + data[i].fid + '>' + data[i].tid + ':' + data[i].content + '</p>';
                $("#history").append(html);
            }
        }
    </script>
</head>
<body>
<div style="width:1000px;margin:0 auto;">
<div id="history" style="overflow-y: scroll;border: 1px solid #ccc; width: 600px; height: auto;height:400px;margin:0 auto;">

</div>
    <div style="width:100%;display: flex;height:100px;align-items: center;justify-content: center;"><input type="text" id="content" style="width:200px;">
        <button onclick="sendMsg()">发送</button></div>
</div>
</body>
</html>