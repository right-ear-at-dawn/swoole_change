<?php


class swoole{

     var $server;

    public function __construct()
    {
        //实例化swoole_websocket_server并存储在我们Chat类中的属性上，达到单例的设计
        $this->server = new Swoole\WebSocket\Server('0.0.0.0', 9502);
        //监听连接事件
        $this->server->on('open', [$this, 'onOpen']);
        //监听接收消息事件
        $this->server->on('message', [$this, 'onMessage']);
        //监听关闭事件
        $this->server->on('close', [$this, 'onClose']);
        //设置允许访问静态文件
        $this->server->set([
            'document_root' => '/grx/swoole/public',//这里传入静态文件的目录
            'enable_static_handler' => true//允许访问静态文件
        ]);
        //开启服务
        $this->server->start();
    }


    /**
     * 连接成功回调函数
     * @param $server
     * @param $request
     */
    public function onOpen($server, $request)
    {
        echo $request->fd . '连接了' . PHP_EOL;//打印到我们终端
    }

    /**
     * 接收到信息的回调函数
     * @param $server
     * @param $frame
     */
    public function onMessage($server, $frame)
    {
        echo $frame->fd . '来了，说：' . $frame->data . PHP_EOL;//打印到我们终端
        foreach ($server->connections as $fd) {//遍历TCP连接迭代器，拿到每个在线的客户端id
            //将客户端发来的消息，推送给所有用户，也可以叫广播给所有在线客户端
            $server->push($fd, json_encode(['no' => $frame->fd, 'msg' => $frame->data]));
        }
    }

    /**
     * 断开连接回调函数
     * @param $server
     * @param $fd
     */
    public function onClose($server, $fd)
    {
        echo $fd . '走了' . PHP_EOL;//打印到我们终端
    }

}

$obj=new swoole();
