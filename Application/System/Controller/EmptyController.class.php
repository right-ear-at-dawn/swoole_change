<?php
/**
 * Copyright (c) 2015-2016 http://www.uminicmf.com All rights reserved.
 * Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * Author：      杜二红 <1186969412@qq.com>
 * Created by:  2018/08/01
 *
 * ----------------------------
 * 空控制器操作
 * ----------------------------
 */
namespace System\Controller;
use Think\Controller;
class EmptyController extends SystemController {
    public function index(){
        echo "空控制器操作";
    }
}