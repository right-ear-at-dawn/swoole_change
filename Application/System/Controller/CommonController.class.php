<?php
/**
 * Copyright (c) 2015-2016 http://www.uminicmf.com All rights reserved.
 * Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * Author：      杜二红 <1186969412@qq.com>
 * Created by:  2018/08/01
 *
 * ----------------------------
 * 后台常用接口库
 * 只认证用户登录权限
 * ----------------------------
 */
namespace System\Controller;
use Think\Controller;
class SystemController extends Controller {
    public function _initialize(){
        header("Content-Type:text/html; charset=utf-8");
        // 用户登录权限认证
        // 如果模块要加载登录认证，必须加载认证方法 user_auth();
        $is_login=user_auth();
        if (!$is_login) {
            $this->redirect('/Auth/Index/login','',2, '亲，还未登录,2s后跳转到登录界面');
        }
    }
}
