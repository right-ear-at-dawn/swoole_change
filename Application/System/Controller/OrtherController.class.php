<?php
/**
 * Copyright (c) 2015-2016 http://www.uminicmf.com All rights reserved.
 * Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * Author：      杜二红 <1186969412@qq.com>
 * Created by:  2018/08/01
 *
 * ----------------------------
 * 系统其他
 * ----------------------------
 */
namespace System\Controller;
use Think\Controller;
class OrtherController extends SystemController {
    public function index(){
    	$this->display();
    }

    public function region_lists(){
        $model_data=D("nation");
        // 搜索重组
        $this->list_data=$model_data->select();
        // 获取当前模型
        // 将单引号转化为双引号，将字符转化为数组
        // 树形菜单分三级 顶级 一级 二级。暂时没有无限级
        // 目前循环数据库中菜单表中所有菜单，有权限后循环赋值了权限的菜单
        // 导航菜单
        $this->nav_menu=get_region_node('pid=1');

        $act_map=array('type'=>3,'pid'=>$this->PUB_THISMENU['id']);
        $this->action_lists=get_node($act_map);

        $add_map=array('type'=>4,'pid'=>$this->PUB_THISMENU['id']);
        $this->action_add=get_node($add_map,'find');

        $str=str_replace("'", '"', $this->THIS_MODEL['list_filed']);
        $this->title_lists=json_decode($str,true);
        $this->displayAuto($_GET['menu_id']);
    }


    public function site($value='')
    {
        $this_site=M('site')->find();
        $redirect_url="/System/Orther/site";
        if (isset($this_site) and !empty($this_site)) {
            if (!isset($_GET['id']) and empty($_GET['id'])) {
                $redirect_url="/System/Orther/site&id=".intval($this_site['id']);
                $this->redirect($redirect_url);
            }
            $this->update(false,false,$redirect_url);
        }
        else{
            $otdata=false;
            $this->add($otdata,$redirect_url);
        }
    }



     // 升级数据库。
    public function update_db(){
        $current_version=M('db_version')->order('id desc')->find()['time_version'];
        //提交的处理
        if (IS_POST) {
            $path=I('post.path');
            //非文件
            if (!is_file($path)) {
                clearstatcache();
                ajaxReturn("",1,"文件不存在");
            }
            
            $file_version=date("YmdHis",filemtime($path));
            if ($current_version>=$file_version) { //选择的版本太低，不能更新
                ajaxReturn("",2,'选择的版本太低，不能更新');
            } 
            
            $Model= M();    
            $tables = $Model -> query('show table status from mkqp');
            // 删除所有表
            foreach ($tables as $row) {
                $Model -> execute('drop table '.$row['name']);
            }
            // 导入
            $sql_file = file_get_contents($path);
            $Model -> execute($sql_file);

            // 更新数据库版本
            $ddata=array();
            $ddata['time_version']=$file_version;
            $ddata['create_time']=date('YmdHis');
            M('db_version')->where('id=1')->save($ddata);
            ajaxReturn("",0,"更新成功！");
        }
        // 1.获取所有文件 current_version
        $root='./db';
        $file_list=scandir($root);
        $db_list=array();
        foreach ($file_list as $row) {
            if (pathinfo($row)['extension']=="sql") {
                $temp=array();
                $path=$root.'/'.$row;
                $temp['path']=$path;
                $temp['name']=$row;
                $temp['is_upgrade']=1;
                $file_version=date("YmdHis",filemtime($path));
                if ($current_version >= $file_version) {
                     $temp['is_upgrade']=0;
                }                
                $temp['version']=$file_version;
                $db_list[$temp['version']]=$temp;
            }

        }
        arsort($db_list);
        // print_r($db_list);
        $this->db_list=$db_list;
        $this->display('System:update_db');
    }

    /**
     * 批量修改表前缀
     */
    function update_db_prefix(){
        $new_prefix="umini";
        $Model = new \Think\Model();
        $all_tables = $Model->query("show tables");
        foreach ($all_tables as $key => $value) {
            // print_r($value);
            $old_name=$value["tables_in_umini"];
            $new_name_arr=explode("_",$old_name);
            $new_name=$new_prefix.substr($old_name,strlen($new_name_arr[0]));
            // echo $new_name;
            // echo "<br />";
            $sql="RENAME TABLE ".$old_name." TO ".$new_name.";";
            $Model->execute("$sql");
        }
        echo "修改完成";
    }
    



}
