<?php
/**
 * Copyright (c) 2015-2016 http://www.uminicmf.com All rights reserved.
 * Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * Author：      杜二红 <1186969412@qq.com>
 * Created by:  2018/08/01
 *
 * ----------------------------
 * 站点配置
 * ----------------------------
 */

namespace Site\Controller;
use Think\Controller;
// 跨模块继承
use System\Controller\SystemController;
// header('Access-Control-Allow-Origin:*'); 

class ConfigController extends SystemController{
  public function index()
  {
  	$this->display('Site:index');
  }

  public function info()
  {
  	$_GET['id']=1;
  	$bak_url=U("Site/Config/info");
  	$this->update(false,false,$bak_url);
  }


}
