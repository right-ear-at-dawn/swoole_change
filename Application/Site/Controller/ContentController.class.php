<?php
/**
 * Copyright (c) 2015-2016 http://www.uminicmf.com All rights reserved.
 * Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * Author：      杜二红 <1186969412@qq.com>
 * Created by:  2018/08/01
 *
 * ----------------------------
 * 内容管理
 * ----------------------------
 */
namespace Site\Controller;
use Think\Controller;
use System\Controller\SystemController;
class ContentController extends SystemController{
  public function manage()
  {
    $menu_id=I('get.menu_id');
    $smap=array();
    $smap['id']=$menu_id;
    $site_menu=M("m_menu")->where($smap)->find();
    $this->show_site_menu=$site_menu;
    if (!$site_menu) {
      echo "非法操作";
      exit();
    }
    $this->THIS_MODEL=M("model")->where("model_name='".$site_menu['model']."'")->find();
    $model_data=M($this->THIS_MODEL['table_name']);
    if ($site_menu['sys_template']=="result") {
      $this->display_page="Site:result";
      // 检测是否有数据，没有就是新增，否则为修改
      $bak_url=U("Site/Content/manage",array("menu_id"=>$menu_id));
      $map=array();
      $map["menu_id"]=$menu_id;
      $exist_data=$model_data->where($map)->find();
      $tdata=array();
      $tdata['menu_id']=$menu_id;
      if ($exist_data) {
        $_GET['id']=$exist_data['id'];
        $this->update($tdata,$map,$bak_url);
      } else {
        $this->add($tdata,$bak_url);
      }
      
    }

    elseif ($site_menu['sys_template']=="lists") {
      $this->list_img_show="img";
      $this->display_page="System:lists";
      $map["menu_id"]=$menu_id;
      $this->lists($map,false,"Site:lists");
    }
    
  }


  function manage_add(){
    $this->display_page="Site:result";
    $menu_id=I('get.menu_id');
    $smap=array();
    $smap['id']=$menu_id;
    $site_menu=M("m_menu")->where($smap)->find();
    $this->THIS_MODEL=M("model")->where("model_name='".$site_menu['model']."'")->find();
    $tdata=array();
    $tdata['menu_id']=$menu_id;
    $bak_url=U("Site/Content/manage",array("menu_id"=>$menu_id));
    $this->add($tdata,$bak_url);
  }


  function manage_update(){
    $this->display_page="Site:result";
    $menu_id=I('get.menu_id');
    $smap=array();
    $smap['id']=$menu_id;
    $site_menu=M("m_menu")->where($smap)->find();
    $this->THIS_MODEL=M("model")->where("model_name='".$site_menu['model']."'")->find();
    $tdata=array();
    $tdata['menu_id']=$menu_id;
    $bak_url=U("Site/Content/manage",array("menu_id"=>$menu_id));

    $map=array();
    $map["menu_id"]=$menu_id;

    $this->update($tdata,$map,$bak_url);
  }

  function manage_delete(){
    $menu_id=I('get.menu_id');
    $smap=array();
    $smap['id']=$menu_id;
    $site_menu=M("m_menu")->where($smap)->find();
    $this->THIS_MODEL=M("model")->where("model_name='".$site_menu['model']."'")->find();
    $bak_url=U("Site/Content/manage",array("menu_id"=>$menu_id));
    $this->delete($smap,$bak_url);
  }
}
