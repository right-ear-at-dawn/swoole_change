<?php
/**
 * Copyright (c) 2015-2016 http://www.uminicmf.com All rights reserved.
 * Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * Author：      杜二红 <1186969412@qq.com>
 * Created by:  2018/08/01
 *
 * ----------------------------
 * 菜单管理
 * ----------------------------
 */
namespace Site\Controller;
use Think\Controller;
use System\Controller\SystemController;
class MenuController extends SystemController{
  public function index()
  {
  	$this->display('Site:index');
  }
  public function menu_lists()
  {
  	// 数据
  	$model_map=array();
  	$model_map['model_name']='m_menu';
  	$model_data=M('model')->where($model_map)->find();
    $orderby="orderid asc,id asc";
  	$map=array();
    $list_data=M('m_menu')->where($map)->order($orderby)->select();

    // 表单
    $art_str=toAry($model_data['list_filed']);
    $this->title_lists=json_decode($art_str,true);

    // 获取当前菜单栏目所在的所有操作方法(比如修改 删除)
    $act_map=array('type'=>3,'pid'=>$this->PUB_THISMENU['id']);
    $this->action_lists=get_menu($act_map);

    $add_map=array('type'=>4,'pid'=>$this->PUB_THISMENU['id']);
    $this->action_add_button=get_menu($add_map,'find');


  	$this->assign('list_data',$list_data);
  	$this->display('Site:menu_lists');
  }

  // 更新
  public function menu_update()
  {
      $this->update();
  }

  // 新增一级菜单
  public function menu_add()
  {
    $this->add();
  }

  // 增加下一级
  public function menu_next_add()
  {
    $data=array();
    $data['parent_id']=I('get.id');
    $this->add($data);
  }

  // 删除
  public function menu_delete()
  {
    $id=I('get.id');
    $map=array();
    $map['parent_id']=$id;
    $ret=M('m_menu')->where($map)->find();
    if ($ret) {
      session('act_info','有下级节点，无法删除！');
      redirect(U('Site/Menu/menu_lists'));
    }
    
    $this->delete(false,U('Site/Menu/menu_lists'));
  }


}





