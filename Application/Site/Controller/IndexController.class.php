<?php
/**
 * Copyright (c) 2015-2016 http://www.uminicmf.com All rights reserved.
 * Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * Author：      杜二红 <1186969412@qq.com>
 * Created by:  2018/08/01
 *
 * ----------------------------
 * 首页
 * ----------------------------
 */
namespace Site\Controller;
use Think\Controller;
use System\Controller\SystemController;
class IndexController extends SystemController{
  public function index()
  {
  	$this->display('Site:index');
  }
  public function info()
  {
  }
}





