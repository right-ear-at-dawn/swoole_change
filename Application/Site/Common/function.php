<?php  
/**
 * Copyright (c) 2015-2016 http://www.uminicmf.com All rights reserved.
 * Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
 * Author：      杜二红 <1186969412@qq.com>
 * Created by:  2018/08/01
 *
 * ----------------------------
 * 网站后台 站点管理 主函数库
 * ----------------------------
 */
function set_tree_style($grade){
	$style="<span>╚</span> ";
	for ($i=0; $i <$grade ; $i++) {
		$style='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$style;
	}
	return "<span style='letter-spacing: -2;margin-right: 3px;'>".$style."</span>";
}

function get_tree($title_lists,$action_lists,$catModel,$pid)
{
    $cat_list=$catModel->where('parent_id='.$pid)->order('id asc')->select();

    foreach ($cat_list as $keyy => $rowy) {
	    echo "<tr>";
	    foreach ($title_lists as $key2 => $row2) {
	        echo "<td>".set_tree_style($rowy['grade']).$rowy[$key2]."</td>";
	    }
	    echo  "<td>";
	    foreach ($action_lists as $act_key => $act_row) {
	    	echo "<a href='/".$act_row['node_name']."&id=".$rowy['id']."' onclick='".$act_row['pre_func']."();'>";
	    	echo $act_row['title'];
	    	echo "</a>&nbsp;";
	    }
	    echo "</td>";
	    echo "</tr>";
	    $cat_list3=$catModel->where('parent_id='.$rowy['id'])->order('id asc')->select();
	    if ($cat_list3) {
			get_tree($title_lists,$action_lists,$catModel,$rowy['id']);
		}
    }
}


function menu_tree($list,$pk="id",$pid="parent_id",$child="_child",$root=0)
{
	$tree=array();
	$Temparr=array(); //定义临时数组

	// 1、建立以id为键值的数组；
	foreach ($list as $row) {
		$Temparr[$row[$pk]]=$row;
	}

	foreach ($Temparr as $key1 => $row1) {
		// 将一级栏目加入tree
		if ($row1[$pid]==$root) {
			$tree[]=& $Temparr[$key1]; //tree 跟temparr将引用同一个地址
		}
		else{
			// 为当前值的父类增加多维数组
			$Temparr[$row1[$pid]]["_child"][]=& $Temparr[$key1];
			$lemp=$Temparr[$row1[$pid]]["_child"];
		}
	}
	return $tree;
}





// 计算层级
function set_tree_lev(&$list,$root=true)
{
	if(is_array($list)){
	 foreach ($list as $key=>$row)
	 {
	 	if ($root) {
	 		$list[$key]['lev']=0;
	 	}
	   if(is_array($list[$key]['_child'])){
	   	foreach ($list[$key]['_child'] as $key1 => $value1) {
	   		$list[$key]['_child'][$key1]['lev']=$list[$key]['lev']+1;
	   	}
	     set_tree_lev($list[$key]['_child'],false);
	   }
	 }
	}
}

function set_tree($tree,$title_lists,$action_lists,$model,$root=true)
{
	foreach ($tree as $row) {
		if ($root) {
			echo "<tr style='background:#f8f8f8'>";
		}
		else{
			echo "<tr>";
		}
		    foreach ($title_lists as $key2 => $row2) {
		    	if ($key2=="cat_name") {
		    		echo "<td>".set_tree_style($row['lev']).get_model_value($row[$key2],$model,$key2)."</td>";
		    	}
		    	else{
		    		echo "<td>".get_model_value($row[$key2],$model,$key2)."</td>";
		    	}
		    }

		    echo  "<td width='280'>";
		    foreach ($action_lists as $act_key => $act_row) {
		    	$actt_name=explode("/",$act_row['node_name']);
		    	if ($actt_name[2]=='add_under_menu') {//添加下级栏目
		    		if ($row['is_lower']!=1) {
		    			continue;
		    		}
		    	}

		    	if ($actt_name[2]=='manage_content') {//添加下级栏目
		    		if ($row['is_manage']!=1) {
		    			continue;
		    		}
		    	}

		    	echo "<a href='".U($act_row['node_name'],array('id'=>$row['id']))."' onclick='".$act_row['pre_func']."();' class='abtn'>";
		    	echo "<i class='".$act_row['img']."'></i>";
		    	echo $act_row['title'];
		    	echo "</a>&nbsp;";
		    }
		    echo "</td>";
		echo "</tr>";
		if ($row['_child']) {
			set_tree($row['_child'],$title_lists,$action_lists,$model,false);
		}
	}
}



function set_site_menu($tree,$root=true)
{
	foreach ($tree as $row) {
		if ($root) {
			echo "<li class='mhed'>";
			if ($row['is_menu']==1) {
				$icon="iconfont icon-liebiao";
			}
			else{
				$icon="iconfont icon-yingyong";
			}
		}
		else{
			echo "<li>";
		}

		if ($row['is_manage']==1) {
			echo "<i class='".$icon."'></i><a style='color:#333' href='".U('Site/Content/content_lists',array('menu_id'=>$row['id']))."'>".set_tree_style($row['lev']).$row['cat_name']."</a>";
		}
		else{
			echo "<i class='".$icon."'></i><a style='color:#898989;'>".set_tree_style($row['lev']).$row['cat_name']."</a>";
		}
		echo "</li>";
		if ($row['_child']) {
			set_site_menu($row['_child'],false);
		}
	}
}


// 检测值为true or false
function val_bool($v){
	if ($v==0) {
		return '否';
	} 
	elseif ($v==1) {
		return '是';
	}
	else{
		return '空';
	}
}

// -------------------self--------------