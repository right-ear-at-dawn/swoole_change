<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.森活城市互娱.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 杜二红 <leeleeleo@qq.com>
// +----------------------------------------------------------------------
// | Created by: 2015-10-11 00:00:00
// +----------------------------------------------------------------------

//----------------------------------
// 移动端入口
//----------------------------------
namespace Wap\Controller;
use Think\Controller;
// 跨模块继承
class IndexController extends Controller{
	function index()
	{
		$this->display("index");
	}

	function saichang(){
		$this->display("saichang");
	}

	function jiaru(){
		$this->display("jiaru");
	}

	function daili(){
		$this->display("daili");
	}

	function about(){
		$this->display("about");
	}
}





